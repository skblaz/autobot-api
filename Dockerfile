
FROM tiangolo/uwsgi-nginx-flask:python3.8
COPY ./app /app
COPY ./nltk_data /local/nltk_data
WORKDIR /app
RUN ls
RUN pip install -r /app/requirements.txt
EXPOSE 5000
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "5000"]
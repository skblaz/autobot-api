from typing import Optional
from fastapi import FastAPI
import time
import autoBOTLib
import glob

#print(list(glob.glob("./*")))

model_path = "./app/model_database/english_model.pickle"
autobotObj = autoBOTLib.load_autobot_model(model_path)
app = FastAPI()

@app.get("/")
def read_root():
    return {"Test": "Success"}

@app.get("/model_generic/{document}")
def read_item(document: str, delimiter: Optional[str] = None):
    
    if not delimiter is None:
        document = document.split(delimiter)
        
    else:
        document = [document]
        
    start_time = time.monotonic()
    
    prediction_obj = autobotObj.predict_proba(document)
    
    end_time = time.monotonic() - start_time
    
    return {"Documents" : document,
            "Class" : prediction_obj,
            "Inference time": end_time}

# A simple autoBOT model API

By running `bash build_docker.sh` an image is created and run, offering simple prediction API for a given pickled autobot model.

## Using own models

Simply replace the model in the `model_database` in `app` and change the path (if needed) in `main.py`.


## Example call

```
http://localhost:5000/model_generic/This%20is%20some%20very%20generic%20text%20%7C%7C%7C%20and%20this%20too?delimiter=|||
```

Should e.g., return

```
{"Documents":["This is some very generic text "," and this too"],"Class":{"0":{"0":0.0,"1":1.0},"1":{"0":1.0,"1":0.0}},"Inference time":0.2254876800002421}
```